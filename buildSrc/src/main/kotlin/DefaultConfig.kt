object DefaultConfig {
    val compileSdkVersion = 30
    val buildToolsVersion = "30.0.2"
    val minSdkVersion = 22
    val targetSdkVersion = 30
    val applicationId = "locle.android.com.source"
}

object ProductFlavour {
    val prod = "Prod"
    val stag = "Stag"
    val dev = "Dev"
}