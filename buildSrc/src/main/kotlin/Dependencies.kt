object Versions {
    const val kotlinVersion = "1.4.30"
    const val coreKtxVersion = "1.2.0"
    const val appcompatVersion = "1.1.0"
    const val materialVersion = "1.1.0"
    const val constraintLayoutVersion = "1.1.3"
    const val jUnitVersion = "4.+"
    const val jUnitExtVersion = "1.1.1"
    const val espressoVersion = "3.2.0"
    const val hiltVersion = "2.33-beta"
    const val buildGradleVersion = "4.1.2"
    const val coroutineVersion = "1.4.2"
    const val coroutineViewModelKTXVersion = "2.3.0"
    const val leakCanary = "2.7"
    const val navigation = "2.2.2"
    const val firebaseBom = "26.8.0"
    const val okhttpBom = "4.9.0"
    const val retrofit = "2.7.1"
}

object Plugins {
    const val androidApp = "com.android.application"
    const val kotlinAndroid = "android"
    const val kotlinKapt = "kapt"
    const val extensions = "kotlin-android-extensions"
    const val daggerHilt = "dagger.hilt.android.plugin"
    const val firebase = "com.google.gms.google-services"
    const val crashlytics = "com.google.firebase.crashlytics"
}

object ClassPaths {
    const val gradle = "com.android.tools.build:gradle:${Versions.buildGradleVersion}"
    const val kotlinGradlePlugin =
        "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.buildGradleVersion}"
}

object Dependencies {
    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib:${Versions.kotlinVersion}"
    const val coreKtx = "androidx.core:core-ktx:${Versions.coreKtxVersion}"
    const val appCompat = "androidx.appcompat:appcompat:${Versions.appcompatVersion}"
    const val material = "com.google.android.material:material:${Versions.materialVersion}"
    const val jUnit = "junit:junit:${Versions.jUnitVersion}"
    const val jUnitKtx = "androidx.test.ext:junit:${Versions.jUnitExtVersion}"
    const val espressoCore = "androidx.test.espresso:espresso-core:${Versions.espressoVersion}"
    const val constrainLayout =
        "androidx.constraintlayout:constraintlayout:${Versions.constraintLayoutVersion}"

    //hilt
    const val hiltAndroid = "com.google.dagger:hilt-android:${Versions.hiltVersion}"
    const val hiltCompiler = "com.google.dagger:hilt-android-compiler:${Versions.hiltVersion}"
    const val hiltTesting = "com.google.dagger:hilt-android-testing:${Versions.hiltVersion}"
    const val hiltAndroidxCompiler = "androidx.hilt:hilt-compiler:${Versions.hiltVersion}"

    //coroutine
    const val coroutinesCore =
        "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutineVersion}"
    const val coroutinesAndroid =
        "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutineVersion}"
    const val coroutinesViewModel =
        "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.coroutineViewModelKTXVersion}"
    const val coroutinesLiveData =
        "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.coroutineViewModelKTXVersion}"

    //leak canary
    const val leakCanary = "com.squareup.leakcanary:leakcanary-android:${Versions.leakCanary}"

    //navigaiton
    const val navigationFragment = "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}"
    const val navigationUi = "androidx.navigation:navigation-ui-ktx:${Versions.navigation}"
    //firebase
    const val firebaseBom = "com.google.firebase:firebase-bom:${Versions.firebaseBom}"
    const val crashlyticsKtx = "com.google.firebase:firebase-crashlytics-ktx"
    const val analyticsKtx = "com.google.firebase:firebase-analytics-ktx"

    //network
    const val okhttpBom ="com.squareup.okhttp3:okhttp-bom:${Versions.okhttpBom}"
    const val okhttp ="com.squareup.okhttp3:okhttp"
    const val loggingInterceptor ="com.squareup.okhttp3:logging-interceptor"
    //retrofit
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val retrofitMoshiConverter = "com.squareup.retrofit2:converter-moshi:${Versions.retrofit}"


}
