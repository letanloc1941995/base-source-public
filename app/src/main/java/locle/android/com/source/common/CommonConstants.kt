package locle.android.com.source.common

object CommonConstants {
}

object NetworkConstants {
    const val CONNECTION_TIME_OUT = 60000L
    const val READ_TIMEOUT = 60000L
    const val WRITE_TIMEOUT = 60000L
}