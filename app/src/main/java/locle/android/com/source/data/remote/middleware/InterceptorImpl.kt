package locle.android.com.source.data.remote.middleware

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException

class InterceptorImpl : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val builder = bindHeader(chain)
        val request = builder.build()
        return chain.proceed(request)
    }

    private fun bindHeader(chain: Interceptor.Chain): Request.Builder {
        val originRequest = chain.request()
        return originRequest.newBuilder()
            .header(ACCEPT, "application/json")
            .addHeader(CACHE_CONTROL, "no-cache")
            .addHeader(CACHE_CONTROL, "no-store")
            .addHeader(KEY_TOKEN, TOKEN_TYPE +"")
    }

    companion object {
        private const val TOKEN_TYPE = "Bearer "
        private const val KEY_TOKEN = "Authorization"
        private const val CACHE_CONTROL = "Cache-Control"
        private const val ACCEPT = "Accept"
    }
}