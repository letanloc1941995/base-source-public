package locle.android.com.source.repository

import locle.android.com.source.common.base.BaseRepository
import locle.android.com.source.data.local.SharedPreferencesManager
import javax.inject.Inject

interface UserRepository {
    fun saveToken(token: String)
}

class UserRepositoryImp @Inject constructor(private val prefer: SharedPreferencesManager) :
    UserRepository, BaseRepository() {
    override fun saveToken(token: String) {
        prefer[TOKEN] = token
    }


    companion object {
        private const val TOKEN = "TOKEN"
    }
}
