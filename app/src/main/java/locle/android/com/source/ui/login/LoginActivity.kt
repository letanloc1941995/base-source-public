package locle.android.com.source.ui.login

import dagger.hilt.android.AndroidEntryPoint
import locle.android.com.source.common.base.BaseActivity
import locle.android.com.source.data.local.SharedPreferencesManager
import locle.android.com.source.databinding.ActivityLoginBinding
import locle.android.com.source.extention.showSoftKeyboard

@AndroidEntryPoint
class LoginActivity :
    BaseActivity<LoginViewModel, ActivityLoginBinding>(LoginViewModel::class.java) {
    override fun getViewBinding() = ActivityLoginBinding.inflate(layoutInflater)
    override fun bindView() {
        binding {
            edtUserName.showSoftKeyboard()
        }
    }
    override fun bindObservable() {
    }
}