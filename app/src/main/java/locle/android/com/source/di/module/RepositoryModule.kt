package locle.android.com.source.di.module

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.components.SingletonComponent
import locle.android.com.source.data.local.SharedPreferencesManager
import locle.android.com.source.repository.UserRepository
import locle.android.com.source.repository.UserRepositoryImp

@InstallIn(ActivityComponent::class)
@Module
class RepositoryModule {

    @Provides
    fun providerUserRepository(preferences: SharedPreferencesManager): UserRepository =
        UserRepositoryImp(preferences)
}