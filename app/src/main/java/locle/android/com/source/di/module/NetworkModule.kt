package locle.android.com.source.di.module

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import locle.android.com.source.BuildConfig
import locle.android.com.source.common.NetworkConstants
import locle.android.com.source.data.remote.middleware.ConnectionInterceptor
import locle.android.com.source.data.remote.middleware.InterceptorImpl
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.inject.Singleton
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.X509TrustManager

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Provides
    fun provideOkHttpClient(
        cache: Cache,
        interceptor: Interceptor,
        sslSocketFactory: SSLSocketFactory,
        trustAllCerts: X509TrustManager,
        connectivityInterceptor: ConnectionInterceptor
    ) = OkHttpClient.Builder().apply {
        cache(cache)
        addInterceptor(interceptor)
        readTimeout(NetworkConstants.READ_TIMEOUT, TimeUnit.SECONDS)
        writeTimeout(NetworkConstants.WRITE_TIMEOUT, TimeUnit.SECONDS)
        connectTimeout(NetworkConstants.CONNECTION_TIME_OUT, TimeUnit.SECONDS)
        sslSocketFactory(sslSocketFactory, trustAllCerts)
        addInterceptor(connectivityInterceptor)
        hostnameVerifier(HostnameVerifier { _, _ -> true })
        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            addInterceptor(logging)
            logging.level = HttpLoggingInterceptor.Level.BODY
        }
    }.build()

    @Provides
    @Singleton
    fun providerInterceptor(): Interceptor = InterceptorImpl()

    @Provides
    @Singleton
    fun providerBaseApiService(okHttpClient: OkHttpClient, moshi: Moshi) = Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .client(okHttpClient)
        .build()

    @Provides
    @Singleton
    fun providerConnectivityInterceptor(@ApplicationContext app: Context): ConnectionInterceptor =
        ConnectionInterceptor(app)

    @Provides
    @Singleton
    @SuppressLint("TrustAllX509TrustManager")
    fun providerX509TrustManager(): X509TrustManager {
        // Create a trust manager that does not validate certificate chains
        return object : X509TrustManager {
            @Throws(CertificateException::class)
            override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {
            }

            @Throws(CertificateException::class)
            override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {
            }

            override fun getAcceptedIssuers(): Array<X509Certificate> {
                return arrayOf()
            }
        }
    }

    @Provides
    @Singleton
    fun providerSslSocketFactory(trust: X509TrustManager): SSLSocketFactory {
        // Install the all-trusting trust manager
        val sslContext = SSLContext.getInstance("SSL")
        sslContext.init(null, arrayOf(trust), java.security.SecureRandom())
        // Create an ssl socket factory with our all-trusting manager
        return sslContext.socketFactory
    }

    @Provides
    @Singleton
    fun provideOkHttpCache(app: Application): Cache {
        val cacheSize: Long = 10 * 1024 * 1024 // 10 MiB
        return Cache(app.cacheDir, cacheSize)
    }
}
