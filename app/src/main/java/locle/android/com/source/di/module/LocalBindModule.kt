package locle.android.com.source.di.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import locle.android.com.source.data.local.SharedPreferencesManager

@Module
@InstallIn(SingletonComponent::class)
class LocalBindModule {
    fun providerSharedPrefs(app: Application): SharedPreferences {
        return app.getSharedPreferences(
            SHARED_PREFS_NAME, Context.MODE_PRIVATE
        )
    }

    @Provides
    fun providerSharedPreferencesManager(sharedPreferences: SharedPreferences) = SharedPreferencesManager(sharedPreferences)
}

const val SHARED_PREFS_NAME = "SHARED_PREFS_NAME"