package locle.android.com.source.common.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding

abstract class BaseActivity<VM : BaseViewModel, VB : ViewBinding>(private val classViewModel: Class<VM>) :
    AppCompatActivity() {
    lateinit var binding: VB
    lateinit var viewModel: VM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = getViewBinding()
        setContentView(binding.root)
        viewModel = ViewModelProvider(this).get(classViewModel)
        bindView()
        bindObservable()
    }

    protected inline fun binding(block: VB.() -> Unit): VB {
        return binding.apply(block)
    }

    abstract fun bindView()
    abstract fun getViewBinding(): VB
    // setup extent
    open fun bindObservable() {}
}