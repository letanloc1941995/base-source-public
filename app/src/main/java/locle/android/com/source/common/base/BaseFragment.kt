package locle.android.com.source.common.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding

abstract class BaseFragment<VM : ViewModel, VB : ViewBinding>(private val classViewModel: Class<VM>) :
    Fragment() {
    lateinit var viewModel: VM
    lateinit var binding: VB

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = getViewBinding()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(classViewModel)
        bindView()
        bindObservable()
    }

    protected inline fun binding(block: VB.() -> Unit): VB {
        return binding.apply(block)
    }

    abstract fun bindView()
    abstract fun getViewBinding(): VB

    // setup extent
    open fun bindObservable() {}
}