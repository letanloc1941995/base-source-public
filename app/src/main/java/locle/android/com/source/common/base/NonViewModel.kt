package locle.android.com.source.common.base

import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class NonViewModel  @Inject constructor(): BaseViewModel()