package locle.android.com.source.extention

fun Any.className(): String = javaClass.simpleName

//fun <VM : ViewModel> Class<VM>.getViewModelClass(): Class<VM> {
//    val type = (javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[0]
//    return type as Class<VM>
//}