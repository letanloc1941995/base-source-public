package locle.android.com.source.extention

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

fun <T> LiveData<T>.observe(owner: LifecycleOwner, observer: (T) -> Unit) =
    observe(owner, Observer {
        if (it != null) observer.invoke(it)
    })

fun LiveData<String>.isNotEmpty() = value?.isNotEmpty()
fun LiveData<String>.isEmpty() = value?.isEmpty()

fun LiveData<Boolean>.isFALSE() = value?.not() ?: false
fun LiveData<Boolean>.isTRUE() = value ?: false


