package locle.android.com.source.data.local

import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.lifecycle.LiveData
import locle.android.com.source.extention.*
import javax.inject.Inject

class SharedPreferencesManager @Inject constructor(val preferences: SharedPreferences) {

    @Suppress("UNCHECKED_CAST")
    inline fun <reified T : Any> get(key: String, defaultValue: T? = null): T {
        return preferences.run {
            when (T::class) {
                Boolean::class -> getBoolean(key, defaultValue as? Boolean? ?: false) as T
                Float::class -> getFloat(key, defaultValue as? Float? ?: 0.0f) as T
                Int::class -> getInt(key, defaultValue as? Int? ?: 0) as T
                Long::class -> getLong(key, defaultValue as? Long? ?: 0L) as T
                String::class -> getString(key, defaultValue as? String? ?: "") as T
                else -> {
                    if (defaultValue is Set<*>) {
                        getStringSet(key, defaultValue as Set<String>) as T
                    } else {
                        val typeName = T::class.java.simpleName
                        throw Error("Unable to get shared preference with value type '$typeName'. Use getObject")
                    }
                }
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    inline operator fun <reified T : Any> set(
        key: String, value: T
    ) {
        preferences.run {
            edit(commit = false) {
                when (T::class) {
                    Boolean::class -> putBoolean(key, value as Boolean)
                    Float::class -> putFloat(key, value as Float)
                    Int::class -> putInt(key, value as Int)
                    Long::class -> putLong(key, value as Long)
                    String::class -> putString(key, value as String)
                }
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    inline fun <reified T : Any> getLiveData(key: String, defaultValue: T? = null): LiveData<T> {
        preferences.run {
            return when (T::class) {
                String::class -> stringLiveData(key, "")
                Boolean::class -> booleanLiveData(key, false)
                Float::class -> floatLiveData(key, 0f)
                Int::class -> intLiveData(key, 0)
                Long::class -> longLiveData(key, 0L)
//                else -> jsonLiveData(key, gson, T::class.java)
                else -> stringLiveData(key, "")
            } as LiveData<T>

        }
    }


}