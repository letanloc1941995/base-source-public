package locle.android.com.source.common.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import locle.android.com.source.extention.SingleLiveData
import locle.android.com.source.utils.DataResult

abstract class BaseViewModel : ViewModel() {
    private val isLoading by lazy { SingleLiveData<Boolean>() }
    private val exception by lazy { SingleLiveData<Exception>() }
    protected fun <T> viewModelScope(
        liveData: MutableLiveData<T>,
        onLoading: (() -> Unit)? = null,
        onSuccess: ((T) -> Unit)? = null,
        onError: ((Exception) -> Unit)? = null,
        onRequest: suspend CoroutineScope.() -> DataResult<T>
    ) {
        viewModelScope.launch {
            when (val asynchronousTasks = onRequest()) {
                is DataResult.Loading -> {
                    showLoading()
                    onLoading?.invoke()
                }
                is DataResult.Success -> {
                    onSuccess?.invoke(asynchronousTasks.data)
//                        ?: kotlin.run { asynchronousTasks.data.let { liveData.value = it } }
                    hideLoading()
                }
                is DataResult.Error -> {
                    onError?.invoke(asynchronousTasks.exception)
                    exception.value = asynchronousTasks.exception
                    hideLoading()
                }
            }
        }
    }

    private fun showLoading() {
        isLoading.value = true
    }

    private fun hideLoading() {
        isLoading.value = false
    }
}