package locle.android.com.source.ui.splashScreen

import android.content.Intent
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import locle.android.com.source.common.base.BaseActivity
import locle.android.com.source.common.base.NonViewModel
import locle.android.com.source.databinding.ActivitySplashScreenBinding
import locle.android.com.source.ui.login.LoginActivity

@AndroidEntryPoint
class SplashScreenActivity :
    BaseActivity<NonViewModel, ActivitySplashScreenBinding>(NonViewModel::class.java) {
    override fun getViewBinding(): ActivitySplashScreenBinding =
        ActivitySplashScreenBinding.inflate(layoutInflater)

    override fun bindView() {
        CoroutineScope(Dispatchers.Main).launch {
            delay(DELAY)
            withContext(Dispatchers.Main) {
                startActivity(Intent(this@SplashScreenActivity, LoginActivity::class.java))
                finish()
            }
        }
    }

    companion object {
        const val DELAY = 3000L
    }
}