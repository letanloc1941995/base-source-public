package locle.android.com.source.extention

import androidx.appcompat.widget.Toolbar

fun Toolbar.clear() {
    title = null
    subtitle = null
    navigationIcon = null
    setNavigationOnClickListener(null)
    logo = null
    menu.clear()
    setOnMenuItemClickListener(null)
}