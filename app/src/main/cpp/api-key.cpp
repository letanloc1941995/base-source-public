//
// Created by Andrew on 3/8/21.
//
#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring extern JNICALL
Java_locle_android_com_baseapplication_MainActivity_googleAPIKey(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "this is api key";
    return env->NewStringUTF(hello.c_str());
}


extern "C" JNIEXPORT jstring
Java_locle_android_com_baseapplication_MainActivity_example(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "this is api key";
    return env->NewStringUTF(hello.c_str());
}

extern "C" JNIEXPORT jstring
Java_locle_android_com_baseapplication_MainActivity_accessKey(
        JNIEnv *env,
        jobject /* this */) {
    std::string ACCESS = "wZMeGYfSgcNRCbAoyFsovMzuu8PnsR5rxJdYoicR7ew";
    return env->NewStringUTF(ACCESS.c_str());
}

