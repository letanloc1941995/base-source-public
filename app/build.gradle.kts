plugins {

    id(Plugins.androidApp)
    id(Plugins.daggerHilt)

    id(Plugins.firebase)
    id(Plugins.crashlytics)

    kotlin(Plugins.kotlinAndroid)
    kotlin(Plugins.kotlinKapt)
}

android {
    compileSdkVersion(DefaultConfig.compileSdkVersion)
    buildToolsVersion(DefaultConfig.buildToolsVersion)
    flavorDimensions("default")
    defaultConfig {
        minSdkVersion(DefaultConfig.minSdkVersion)
        targetSdkVersion(DefaultConfig.targetSdkVersion)
        applicationId = DefaultConfig.applicationId

        versionCode = Releases.versionCode
        vectorDrawables.useSupportLibrary = true
        versionName = Releases.versionName
        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
    }

    buildFeatures {
        viewBinding = true
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    externalNativeBuild {
        cmake {
            path("src/main/cpp/CMakeLists.txt")
            version = "3.10.2"
        }
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

    signingConfigs {
//        getByName("debug") {
//            keyAlias = "debug"
//            keyPassword = "my debug key password"
//            storeFile = file("/home/miles/keystore.jks")
//            storePassword = "my keystore password"
//        }
        create("release") {
            keyAlias = "alias"
            keyPassword = "123456"
            storeFile = file("../keystore/my_keystore")
            storePassword = "123456"
        }
    }

    productFlavors {
        create("dev") {
            versionCode = Releases.versionCode
            versionName = Releases.versionName
            applicationIdSuffix = ".dev"
            applicationId = DefaultConfig.applicationId
            buildConfigField("String", "BASE_URL", "\"https://pokeapi.co/api/v2/\"")
            resValue("string", "app_name", "base app dev")
        }

        create("stag") {
            applicationIdSuffix = ".stg"
            versionCode = Releases.versionCode
            versionName = Releases.versionName
            buildConfigField("String", "BASE_URL", "\"https://pokeapi.co/api/v2/\"")
            resValue("string", "app_name", "base app stag")
        }

        applicationVariants.all{
            outputs.forEach {output->
                if (output is com.android.build.gradle.internal.api.BaseVariantOutputImpl) {
                    output.outputFileName =
                        "base_source${versionName}_${name}.${output.outputFile.extension}"
                }
            }
        }

    }

    buildTypes {
        getByName("release") {
            signingConfig = signingConfigs.getByName("release")
            proguardFiles("proguard-rules.pro")
            isMinifyEnabled = true
        }

        getByName("debug") {
            debuggable(true)
        }
    }

    // set default source
//    sourceSets["main"].java.srcDir("src/main/kotlin")
}

dependencies {

    implementation(Dependencies.kotlin)
    implementation(Dependencies.coreKtx)
    implementation(Dependencies.appCompat)
    implementation(Dependencies.material)
    implementation(Dependencies.constrainLayout)
    //unit test
    androidTestImplementation(Dependencies.jUnitKtx)
    androidTestImplementation(Dependencies.espressoCore)
    testImplementation(Dependencies.jUnit)
    //hilt
    implementation(Dependencies.hiltAndroid)
    kapt(Dependencies.hiltCompiler)
    //coroutine
    implementation(Dependencies.coroutinesCore)
    implementation(Dependencies.coroutinesAndroid)
    implementation(Dependencies.coroutinesViewModel)
    implementation(Dependencies.coroutinesLiveData)
    //leak canary
    debugImplementation(Dependencies.leakCanary)
    //navigation
    implementation(Dependencies.navigationFragment)
    implementation(Dependencies.navigationUi)
    //firebase
    implementation(platform(Dependencies.firebaseBom))
    implementation (Dependencies.analyticsKtx)
    implementation (Dependencies.crashlyticsKtx)
    //network
    implementation(platform(Dependencies.okhttpBom))
    implementation(Dependencies.okhttp)
    implementation(Dependencies.loggingInterceptor)
    implementation(Dependencies.retrofit)
    implementation(Dependencies.retrofitMoshiConverter)
}
